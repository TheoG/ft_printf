/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 14:44:38 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 20:14:43 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include "libft.h"

# define H 1
# define HH 2
# define L 3
# define LL 6
# define J 4
# define Z 5

typedef struct			s_opt
{
	int					is_opt;
	char				opt;
	int					is_sharp;
	int					first_space;
	int					is_plus;
	int					is_width;
	int					width;
	int					is_precision;
	int					precision;
	int					is_len;
	int					len;
	int					spec;
	char				*str;
}						t_opt;

typedef struct			s_arg
{
	char				*flag;
	int					(*fct)(va_list ag, char flag, t_opt *opt);
}						t_arg;

int						ft_bourrage(char c, int nb);
unsigned long			get_cast_ul(va_list ag, char flag, t_opt *opt);
long long int			get_cast_d(va_list ag, t_opt *opt);
void					analyse_length(t_opt **opt, char *str);
void					analyse_precision(t_opt **opt, char *str);
void					analyse_minimal_width(t_opt **opt, char *str);
t_opt					*ana_opt(char *str, char *format);
int						ft_nbr(va_list ag, char flag, t_opt *opt);
int						ft_big_nbr(va_list ag, char flag, t_opt *opt);
int						ft_nbr_oct(va_list ag, char flag, t_opt *opt);
int						ft_nbr_bin(va_list ag, char flag, t_opt *opt);
int						ft_hex_sign(void);
int						ft_nbr_hex(va_list ag, char flag, t_opt *opt);
int						ft_str(va_list ag, char flag, t_opt *opt);
int						ft_ptr(va_list ag, char flag, t_opt *opt);
int						ft_noarg(va_list ag, char flag, t_opt *opt);

int						ft_printf(const char *restrict format, ...);

static const t_arg g_arg[] =
{
	{"di", &ft_nbr},
	{"b", &ft_nbr_bin},
	{"DuU", &ft_big_nbr},
	{"oO", &ft_nbr_oct},
	{"xX", &ft_nbr_hex},
	{"scSC", &ft_str},
	{"p", &ft_ptr},
	{NULL, NULL}
};

#endif
