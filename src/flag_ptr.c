/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_ptr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/04 16:18:23 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 20:15:47 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		pre_padding(t_opt *opt, int len, unsigned long ptr, int *is_hex)
{
	int	len_opt;

	len_opt = 0;
	if (opt->is_opt && opt->is_opt > len - 2 && (opt->opt == '0'
				|| opt->opt == ' '))
	{
		if (opt->opt == '0' && ptr == 0)
		{
			len_opt += ft_hex_sign();
			*is_hex = 1;
		}
		return (len_opt + ft_bourrage(opt->opt, opt->is_opt - len - 2));
	}
	return (0);
}

int		ft_ptr(va_list ag, char flag, t_opt *opt)
{
	long unsigned	ptr;
	char			*str;
	int				len_opt;
	int				is_hex;
	int				len;

	(void)flag;
	len_opt = 0;
	is_hex = 0;
	ptr = va_arg(ag, long unsigned);
	str = ft_itoa_base_unsigned(ptr, 16);
	len = (int)ft_strlen(str);
	if (opt->is_precision && opt->precision == 0 && ptr == 0)
		return (ft_hex_sign());
	len_opt += pre_padding(opt, len, ptr, &is_hex);
	if (!is_hex)
		len_opt += ft_hex_sign();
	if (opt->is_precision && opt->precision > len)
		len_opt += ft_bourrage('0', opt->precision - len);
	ft_putstr(str);
	if (opt->is_opt > len - 2 && (opt->opt == '-'))
		len_opt += ft_bourrage(' ', opt->is_opt - len - 2);
	len_opt = len_opt > 0 ? len_opt : 0;
	free(str);
	return (ptr == 0 ? len_opt + 1 : len + len_opt);
}
