/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 13:09:44 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 16:12:13 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_bourrage(char c, int nb)
{
	int compteur;

	compteur = nb;
	while (nb-- > 0)
		ft_putchar(c);
	if (compteur < 0)
		return (0);
	return (compteur);
}

unsigned long	get_cast_ul(va_list ag, char flag, t_opt *opt)
{
	unsigned long nb;

	nb = 0;
	if (flag == 'O')
		nb = (unsigned long)va_arg(ag, unsigned long);
	else if (opt->len == 0)
		nb = va_arg(ag, unsigned int);
	else if (opt->len == HH)
		nb = (unsigned char)va_arg(ag, int);
	else if (opt->len == H)
		nb = (unsigned short int)va_arg(ag, int);
	else if (opt->len == L)
		nb = va_arg(ag, unsigned long int);
	else if (opt->len == LL)
		nb = va_arg(ag, unsigned long long int);
	else if (opt->len == J)
		nb = va_arg(ag, uintmax_t);
	else if (opt->len == Z)
		nb = va_arg(ag, size_t);
	return (nb);
}

long long int	get_cast_d(va_list ag, t_opt *opt)
{
	long long i;

	i = 0;
	if (opt->len == 0)
		i = va_arg(ag, int);
	else if (opt->len == HH)
		i = (signed char)va_arg(ag, int);
	else if (opt->len == H)
		i = (short int)va_arg(ag, int);
	else if (opt->len == L)
		i = va_arg(ag, long int);
	else if (opt->len == LL)
		i = va_arg(ag, long long int);
	else if (opt->len == J)
		i = va_arg(ag, intmax_t);
	else if (opt->len == Z)
		i = va_arg(ag, size_t);
	else
		i = va_arg(ag, int);
	return (i);
}
