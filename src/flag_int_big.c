/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_int_big.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 14:16:49 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 16:16:59 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

unsigned long long	cast_value_big(va_list ag, t_opt *opt)
{
	unsigned long long ull;

	ull = 0;
	if (opt->len == 0)
		ull = va_arg(ag, unsigned int);
	else if (opt->len == HH)
		ull = (unsigned char)va_arg(ag, int);
	else if (opt->len == H)
		ull = (unsigned short int)va_arg(ag, int);
	else if (opt->len == L)
		ull = va_arg(ag, unsigned long int);
	else if (opt->len == LL)
		ull = va_arg(ag, unsigned long long int);
	else if (opt->len == J)
		ull = va_arg(ag, uintmax_t);
	else if (opt->len == Z)
		ull = va_arg(ag, size_t);
	return (ull);
}

int					flag_u(va_list ag, t_opt *opt)
{
	int					len;
	int					ret;
	unsigned long long	ull;

	len = 0;
	ret = 0;
	ull = cast_value_big(ag, opt);
	len = ft_size_big_num_u(ull);
	if (opt->is_precision && opt->precision == 0 && ull == 0)
		return (0);
	if (opt->is_opt > len && (opt->opt == '0' || opt->opt == ' '))
		ret += ft_bourrage(opt->opt, opt->is_opt - len -
				((opt->precision - len) > 0 ? opt->precision - len : 0));
	if (opt->is_precision && opt->precision > len)
		ret += ft_bourrage('0', opt->precision - len);
	ft_putbignbr_u(ull);
	if (opt->is_opt > len && (opt->opt == '-'))
		ret += ft_bourrage(' ', opt->is_opt - len -
				((opt->precision - len) > 0 ? opt->precision - len : 0));
	return (len + ret);
}

int					ft_big_nbr(va_list ag, char flag, t_opt *opt)
{
	unsigned long long	ull;
	long long int		lli;
	int					ret;

	ull = 0;
	ret = 0;
	if (flag == 'u')
		return (flag_u(ag, opt));
	else if (flag == 'U')
	{
		ull = va_arg(ag, unsigned long long);
		ft_putbignbr_u(ull);
		return (ft_size_big_num_u(ull));
	}
	else if (flag == 'D')
	{
		lli = va_arg(ag, long long);
		ft_putbignbr(lli);
		return (ft_size_big_num(lli));
	}
	return (0);
}
