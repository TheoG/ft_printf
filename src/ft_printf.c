/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 15:43:39 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 20:08:23 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdarg.h>
#include <wchar.h>

#include "ft_printf.h"

static int		check_spec(char specifier, va_list ag, t_opt *opt)
{
	int i;

	i = 0;
	while (g_arg[i].flag && specifier)
	{
		if (ft_strchr(g_arg[i].flag, specifier))
		{
			i = g_arg[i].fct(ag, specifier, opt);
			free(opt);
			return (i);
		}
		i++;
	}
	i = 0;
	if (opt->is_opt > 0 && (opt->opt == '0' || opt->opt == ' '))
		i += ft_bourrage(opt->opt, opt->is_opt - 1);
	ft_putchar(*(opt->str + 1));
	if (opt->is_opt > 0 && (opt->opt == '-'))
		i += ft_bourrage(' ', opt->is_opt - 1);
	if ((*opt->str + 2))
		ft_putstr(opt->str + 2);
	free(opt);
	return (i);
}

static void		to_the_end(char **format)
{
	while (*(*format) && !ft_strchr("bdDiuUiIoOxXsScCp", *(*format)))
	{
		if (!ft_strchr("#0 +-hljzt.", *(*format)) && !ft_isdigit(*(*format)))
			break ;
		(*format)++;
	}
}

static int		loop(char **format, int *ret, char *str, va_list ag)
{
	if (*(*format) == '%' && (*format)++)
	{
		ft_putchar('%');
		(*ret)++;
	}
	else
	{
		str = (*format);
		to_the_end(format);
		if (!*(*format) || !ft_strchr("bdDiuUiIoOxXsScCp", *(*format)))
		{
			str = ft_strsub(str, 0, ft_strlen(str));
			(*ret) += check_spec(*(*format), ag, ana_opt(str, (*format) - 1));
			free(str);
			return (*ret + ft_strlen((*format)));
		}
		else
		{
			str = ft_strsub(str, 0, (*format) - str);
			(*ret) += check_spec(*(*format), ag, ana_opt(str, (*format) - 1));
		}
		free(str);
		(*format)++;
	}
	return (0);
}

static int		ft_vprintf(char *format, va_list ag)
{
	char		*str;
	int			ret;

	ret = 0;
	str = NULL;
	while (*format)
	{
		if (*format == '%' && format++)
		{
			if (loop(&format, &ret, str, ag) != 0)
				return (ret + ft_strlen(format));
		}
		else
		{
			ft_putchar(*format);
			format++;
			ret++;
		}
	}
	return (ret);
}

int				ft_printf(const char *restrict format, ...)
{
	va_list	ag;
	int		ret;

	va_start(ag, format);
	ret = ft_vprintf((char *)format, ag);
	va_end(ag);
	return (ret);
}
