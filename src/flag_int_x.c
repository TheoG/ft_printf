/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_int_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 14:16:38 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 16:46:50 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_hex_sign(void)
{
	ft_putstr("0x");
	return (2);
}

static int	pre_padding(t_opt *opt, char flag, int nb, int len)
{
	int	ret;
	int	opt_len;

	ret = 0;
	opt_len = opt->is_opt - len - (opt->is_sharp ? 2 : 0)
		- ((opt->precision - len) > 0 ? opt->precision - len : 0);
	if (opt->is_sharp && nb != 0)
	{
		if (flag == 'X')
			ft_putstr("0X");
		else
			ft_putstr("0x");
		ret += 2;
	}
	if (opt->is_opt > len && opt->opt == '0')
		ret += ft_bourrage(opt->opt, opt_len);
	if (opt->is_precision && opt->precision > len)
		ret += ft_bourrage('0', opt->precision - len);
	return (ret);
}

static int	precision(t_opt *opt, char **nb_str)
{
	if (opt->is_opt)
		ft_putchar(' ');
	free(*nb_str);
	return (opt->is_opt > 0 ? opt->is_opt : 0);
}

int			ft_nbr_hex(va_list ag, char flag, t_opt *opt)
{
	char			*nb_str;
	int				ret;
	int				len;
	long unsigned	nb;
	int				opt_len;

	ret = 0;
	nb = get_cast_ul(ag, flag, opt);
	nb_str = ft_itoa_base_unsigned(nb, 16);
	len = (int)ft_strlen(nb_str);
	opt_len = opt->is_opt - len - (opt->is_sharp ? 2 : 0)
		- ((opt->precision - len) > 0 ? opt->precision - len : 0);
	if (flag == 'X')
		nb_str = ft_toupper_str(nb_str);
	if (opt->is_opt > len && opt->opt == ' ')
		ret += ft_bourrage(opt->opt, opt_len);
	if (opt->is_precision && opt->precision == 0 && nb == 0)
		return (precision(opt, &nb_str));
	ret += pre_padding(opt, flag, nb, len);
	ft_putstr(nb_str);
	if (opt->is_opt > len && (opt->opt == '-'))
		ret += ft_bourrage(' ', opt_len);
	free(nb_str);
	return (ret + len);
}
