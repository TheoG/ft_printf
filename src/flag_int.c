/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_int.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/20 18:48:00 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 19:15:03 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			first_step(int i, int *ret, t_opt **opt)
{
	int len;

	len = 0;
	if (i < 0)
	{
		len = -1;
		(*ret)++;
	}
	if ((*opt)->first_space && i >= 0)
	{
		ft_putchar(' ');
		(*opt)->is_opt--;
		(*ret)++;
	}
	if ((*opt)->is_plus && i >= 0 && (*opt)->opt != ' ')
	{
		ft_putchar('+');
		(*ret)++;
		if (i == 0)
			(*opt)->is_opt--;
	}
	return (len);
}

static int	pre_padding(t_opt **opt, long long i, int len, int *taille)
{
	int	ret;
	int	is_neg;

	ret = 0;
	is_neg = i < 0 ? 1 : 0;
	if ((*opt)->is_opt && (*opt)->opt == '0' && (*opt)->is_precision)
		(*opt)->opt = ' ';
	if ((*opt)->is_opt > len && ((*opt)->opt == '0' || (*opt)->opt == ' '))
	{
		*taille = (*opt)->is_opt - len - (i > 0 && (*opt)->is_plus ? 1 : 0)
	- (((*opt)->precision - len) > 0 ? (*opt)->precision - len : 0) - is_neg;
		if (i < 0 && (*opt)->opt == '0')
			ft_putchar('-');
		ret += ft_bourrage((*opt)->opt, *taille);
		if ((*opt)->is_plus && i >= 0 && (*opt)->opt == ' ')
		{
			ft_putchar('+');
			ret++;
			if (i == 0)
				(*opt)->is_opt--;
		}
		if (i < 0 && (*opt)->opt == ' ' && *taille > 0)
			ft_putchar('-');
	}
	return (ret);
}

static int	precision(t_opt *opt, long long i, char *nb, int taille)
{
	int	ret;
	int	is_neg;
	int	len;

	ret = 0;
	is_neg = *nb == '-' ? 1 : 0;
	len = (int)ft_strlen(nb) - is_neg;
	if (opt->is_precision && opt->precision >= len)
	{
		if (is_neg && taille <= 0)
			ft_putchar('-');
		ret += ft_bourrage('0', opt->precision - len);
	}
	if (is_neg && opt->opt == '-')
		ft_putchar('-');
	if (is_neg && ((opt->is_opt && opt->is_opt > len)
			|| (opt->is_precision && opt->precision > len)))
		ft_putstr(nb + 1);
	else
		ft_putstr(nb);
	if (opt->is_opt > len && (opt->opt == '-'))
		ret += ft_bourrage(' ', opt->is_opt - len - is_neg - (i > 0 &&
		opt->is_plus ? 1 : 0) - ((opt->precision - len) > 0 ?
		opt->precision - len : 0));
	return (ret);
}

int			ft_nbr(va_list ag, char flag, t_opt *opt)
{
	long long	i;
	int			ret;
	int			len;
	char		*nb;
	int			taille;

	(void)flag;
	ret = 0;
	taille = 0;
	i = get_cast_d(ag, opt);
	nb = ft_itoa_base_big(i, 10);
	len = (int)ft_strlen(nb);
	if (first_step(i, &ret, &opt) == -1)
		len--;
	ret += pre_padding(&opt, i, len, &taille);
	if (opt->is_precision && opt->precision == 0 && i == 0)
	{
		if (opt->is_opt)
			ft_putchar(' ');
		free(nb);
		return (ret + (opt->is_opt ? 1 : 0));
	}
	ret += precision(opt, i, nb, taille);
	free(nb);
	return (len + ret);
}

int			ft_nbr_bin(va_list ag, char flag, t_opt *opt)
{
	char	*nb;
	int		len;

	(void)opt;
	if (flag == 'b')
	{
		nb = ft_itoa_base(va_arg(ag, int), 2);
		len = (int)ft_strlen(nb);
		ft_putstr(nb);
		return (len);
	}
	return (0);
}
