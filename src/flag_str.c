/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_str.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/11 11:10:42 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 16:11:13 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_null_str(void)
{
	ft_putstr("(null)");
	return (6);
}

int		flag_s(va_list ag, t_opt *opt)
{
	char	*str;
	int		len;
	int		len_opt;

	len = 0;
	len_opt = 0;
	str = va_arg(ag, char *);
	if (!str && !opt->is_opt)
		return (ft_null_str());
	if (opt->is_precision)
		str = ft_strsub(str, 0, opt->precision);
	if (str)
		len = (int)ft_strlen(str);
	if (opt->is_opt > len && (opt->opt == '0' || opt->opt == ' '))
		len_opt += ft_bourrage(opt->opt, opt->is_opt - len);
	ft_putstr(str);
	if (opt->is_opt > len && (opt->opt == '-'))
		len_opt += ft_bourrage(' ', opt->is_opt - len);
	if (opt->is_precision && str)
		free(str);
	return (len + len_opt);
}

int		handle_str(va_list ag, char flag, t_opt *opt)
{
	int		len_opt;

	len_opt = 0;
	if (flag == 's')
		return (flag_s(ag, opt));
	else if (flag == 'c')
	{
		if (opt->is_opt > 1 && (opt->opt == '0' || opt->opt == ' '))
			len_opt += ft_bourrage(opt->opt, opt->is_opt - 1);
		ft_putchar(va_arg(ag, int));
		if (opt->is_opt > 1 && (opt->opt == '-'))
			len_opt += ft_bourrage(' ', opt->is_opt - 1);
	}
	return (1 + len_opt);
}

int		handle_wstr(va_list ag, char flag, t_opt *opt)
{
	wchar_t	*str;
	int		len_opt;

	(void)opt;
	len_opt = 0;
	if (flag == 'S')
	{
		str = va_arg(ag, wchar_t *);
		if (!str)
			return (ft_null_str());
		if (opt->is_precision)
			str = ft_wstrsub(str, 0, opt->precision);
		if (opt->is_opt > (int)ft_wstrlen(str) &&
				(opt->opt == '0' || opt->opt == ' '))
			len_opt += ft_bourrage(opt->opt, opt->is_opt - ft_wstrlen(str));
		ft_putwstr(str);
		if (opt->is_opt > (int)ft_wstrlen(str) && (opt->opt == '-'))
			len_opt += ft_bourrage(' ', opt->is_opt - ft_wstrlen(str));
		if (opt->is_precision)
			free(str);
		return (ft_wstrlen(str) + len_opt);
	}
	else if (flag == 'C')
		return (ft_putwchar(va_arg(ag, unsigned int)));
	return (0);
}

int		ft_str(va_list ag, char flag, t_opt *opt)
{
	if (opt->len == L || flag == 'C' || flag == 'S')
	{
		flag = ft_toupper(flag);
		return (handle_wstr(ag, flag, opt));
	}
	else
		return (handle_str(ag, flag, opt));
}
