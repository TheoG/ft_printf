/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analyse_string.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/20 16:51:36 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 20:05:58 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		analyse_length(t_opt **opt, char *str)
{
	int ret;

	ret = 0;
	while (*str)
	{
		if (*str == 'h')
			ret++;
		else if (*str == 'l')
			ret += 3;
		else if (*str == 'j')
			ret += 4;
		else if (*str == 'z')
			ret += 5;
		str++;
	}
	(*opt)->len = ret;
}

void		analyse_precision(t_opt **opt, char *str)
{
	(*opt)->is_precision = 0;
	while (*str)
	{
		if (*str == '.')
		{
			(*opt)->is_precision = 1;
			(*opt)->precision = ft_atoi(str + 1);
		}
		str++;
	}
}

int			add_option(t_opt **opt, char **str)
{
	if (!(*opt)->is_opt && (*(*str) == '-' || *(*str) == '0' ||
				*(*str) == ' ' || ft_isdigit(*(*str))))
	{
		if (*(*str) == '0' && *((*str) + 1) == '-' && (*str)++)
			return (-1);
		if (*(*str) == '0' || *(*str) == '-')
		{
			(*opt)->opt = *(*str);
			if ((*str) + 1 && *((*str) + 1) == '#')
			{
				(*opt)->is_opt = ft_atoi((*str) + 2);
				(*opt)->is_sharp = 1;
			}
			else
				(*opt)->is_opt = ft_atoi((*str) + 1);
		}
		else if ((*opt)->is_opt != 0)
			(*opt)->is_opt = ft_atoi((*str));
		if (*((*str) + 1) == '+' && (*str)++)
			(*opt)->is_plus = 1;
		(*str) += (*opt)->is_opt != 0 ? ft_size_num((*opt)->is_opt) : 0;
	}
	return (0);
}

void		analyse_minimal_width(t_opt **opt, char *str)
{
	if (*str == '#' && str++)
		(*opt)->is_sharp = 1;
	if (*str == ' ' && str++ && (ft_atoi(str) ||
				ft_strchr("DiuUiIoOxXsScCp", *str)))
		(*opt)->first_space = 1;
	if (*str != '-' && *str != '0' && *str != '+' &&
			((*opt)->is_opt = ft_atoi(str)))
		(*opt)->opt = ' ';
	while (*str)
	{
		if (*str == '#' && str++)
			(*opt)->is_sharp = 1;
		if (*str == '+')
		{
			(*opt)->is_plus = 1;
			str++;
			if (*str != '-' && *str != '0' && *str != '+' &&
					((*opt)->is_opt = ft_atoi(str)))
				(*opt)->opt = ' ';
		}
		if (add_option(opt, &str) == -1)
			continue ;
		str++;
	}
}

t_opt		*ana_opt(char *str, char *format)
{
	t_opt	*opt;

	opt = (t_opt *)malloc(sizeof(t_opt));
	if (!opt)
		return (NULL);
	ft_bzero(opt, sizeof(t_opt));
	analyse_length(&opt, str);
	analyse_precision(&opt, str);
	analyse_minimal_width(&opt, str);
	opt->str = format;
	return (opt);
}
