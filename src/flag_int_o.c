/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flag_int_o.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 14:16:22 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 18:18:05 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	padding(t_opt *opt, int *ret, unsigned long nb, char **nb_str)
{
	int len;

	len = (int)ft_strlen(*nb_str);
	if (opt->is_opt > len && (opt->opt == '0' || opt->opt == ' '))
		*ret += ft_bourrage(opt->opt, opt->is_opt - opt->is_sharp
		- len - ((opt->precision - len) > 0 ? opt->precision - len : 0));
	if (opt->is_sharp)
	{
		ft_putchar('0');
		(*ret)++;
		if (nb == 0)
		{
			free(*nb_str);
			return (-1);
		}
	}
	if (opt->is_precision && opt->precision > len)
		*ret += ft_bourrage('0', opt->precision - len - opt->is_sharp);
	return (0);
}

int			ft_nbr_oct(va_list ag, char flag, t_opt *opt)
{
	int				ret;
	int				len;
	char			*nb_str;
	unsigned long	nb;

	ret = 0;
	nb = get_cast_ul(ag, flag, opt);
	nb_str = ft_itoa_base_unsigned(nb, 8);
	len = (int)ft_strlen(nb_str);
	if (padding(opt, &ret, nb, &nb_str) == -1)
		return (ret);
	if (opt->is_precision && opt->precision == 0 && nb == 0)
	{
		if (opt->is_opt)
			ft_putchar(' ');
		free(nb_str);
		return (ret + (opt->is_opt > 0 ? 1 : 0));
	}
	ft_putstr(nb_str);
	if (opt->is_opt > len - opt->is_sharp && (opt->opt == '-'))
		ret += ft_bourrage(' ', opt->is_opt - opt->is_sharp -
		len - ((opt->precision - len) > 0 ? opt->precision - len : 0));
	free(nb_str);
	return (ret + len);
}
