# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tgros <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/19 15:52:26 by tgros             #+#    #+#              #
#    Updated: 2016/12/20 16:53:27 by tgros            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRC_PATH = src/

SRC_NAME = ft_printf.c flag_ptr.c flag_int.c flag_int_o.c flag_int_x.c \
		   flag_int_big.c flag_str.c util.c analyse_string.c

LIB_NAME = ft_atoi.c ft_bzero.c ft_isdigit.c ft_itoa_base.c ft_itoa_base_big.c \
		   ft_itoa_base_unsigned.c ft_putbignbr.c ft_putbignbr_u.c \
		   ft_putchar.c ft_putstr.c ft_putwchar.c ft_putwstr.c \
		   ft_size_big_num.c ft_size_big_num_u.c ft_size_num.c ft_strchr.c \
		   ft_strlen.c ft_strsub.c ft_toupper.c ft_toupper_str.c \
		   ft_wstrlen.c ft_wstrsub.c ft_memset.c ft_strdup.c ft_strcpy.c

OBJ = $(SRC:.c=.o)

LIB_OBJ = $(LIB_SRC:.c=.o)

CC	= gcc

FLG = -Werror -Wextra -Wall

SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))
LIB_SRC = $(addprefix $(INC_LIB), $(LIB_NAME))

INC_LIB = libft/

INC = includes/

all: $(NAME)

%.o : %.c
	@$(CC) $(FLG) -I$(INC_LIB) -I$(INC) -c -o $@ $^

$(NAME): $(LIB_OBJ) $(OBJ)
	@ar rc $(NAME) $(OBJ) $(LIB_OBJ)
	@ranlib $(NAME)
	@echo "ft_printf: done"

clean:
	/bin/rm -f $(OBJ)
	make -C $(INC_LIB) clean

fclean: clean
	/bin/rm -f $(NAME)
	make -C $(INC_LIB) fclean

re: fclean all
