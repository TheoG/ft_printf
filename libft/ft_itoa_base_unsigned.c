/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_unsigned.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/05 16:51:28 by tgros             #+#    #+#             */
/*   Updated: 2016/12/14 10:05:46 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static	void	reverse(char *str)
{
	char tmp;
	char *end;

	end = str + ft_strlen(str) - 1;
	while (str < end)
	{
		tmp = *str;
		*str = *end;
		*end = tmp;
		str++;
		end--;
	}
}

static char		*get_str(char *str, long unsigned n, int base)
{
	const char *tmp;

	tmp = str;
	while (n != 0)
	{
		if (n % base > 9)
			*str = (n % base) - 10 + 'a';
		else
			*str = (n % base) + '0';
		n = (n / base >> 0);
		str++;
	}
	if (n == 0)
		*str = '0';
	return ((char *)tmp);
}

char			*ft_itoa_base_unsigned(long unsigned n, int base)
{
	unsigned long	tmp;
	int				size;
	char			*res;

	size = 1;
	tmp = n;
	while (tmp /= base)
		size++;
	res = (char *)malloc(sizeof(char) * size + 1);
	if (!res)
		return (NULL);
	get_str(res, n, base);
	res[size] = '\0';
	reverse(res);
	return (res);
}
