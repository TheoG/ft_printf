/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_size_big_num.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 11:06:12 by tgros             #+#    #+#             */
/*   Updated: 2016/12/10 09:28:07 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <limits.h>

int		ft_size_big_num(long long int num)
{
	int res;

	res = 1;
	if (num == LONG_MIN)
		return (20);
	if (num < 0)
	{
		res++;
		num = -num;
	}
	while ((num /= 10) > 0)
		res++;
	return (res);
}
