/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 09:32:50 by tgros             #+#    #+#             */
/*   Updated: 2016/12/12 14:23:05 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

static int	ft_bit_operations(int size, unsigned int c)
{
	if (size <= 11)
	{
		ft_putchar((c >> 6) | 0b11000000);
		ft_putchar(((c << 26) >> 26) | 0b10000000);
		return (2);
	}
	else if (size <= 16)
	{
		ft_putchar((c >> 12) | 0b11100000);
		ft_putchar(((c << 20) >> 26) | 0b10000000);
		ft_putchar(((c << 26) >> 26) | 0b10000000);
		return (3);
	}
	else
	{
		ft_putchar((c >> 18) | 0b11110000);
		ft_putchar(((c << 14) >> 26) | 0b10000000);
		ft_putchar(((c << 20) >> 26) | 0b10000000);
		ft_putchar(((c << 26) >> 26) | 0b10000000);
		return (4);
	}
}

int			ft_putwchar(unsigned int c)
{
	unsigned int	mask;
	int				size;

	mask = 2147483648;
	size = 32;
	if (c == 0)
	{
		ft_putchar(0);
		return (1);
	}
	while ((c & mask) == 0)
	{
		size--;
		mask = mask >> 1;
	}
	if (size <= 7)
		ft_putchar(c);
	else
		return (ft_bit_operations(size, c));
	return (1);
}
