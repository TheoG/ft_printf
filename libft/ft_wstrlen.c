/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 10:28:11 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 15:06:36 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_size(wchar_t c)
{
	int	mask;
	int	size;

	mask = 2147483648;
	size = 32;
	while ((c & mask) == 0)
	{
		size--;
		mask = mask >> 1;
	}
	if (size <= 7)
		return (1);
	else if (size <= 11)
		return (2);
	else if (size <= 16)
		return (3);
	else
		return (4);
}

size_t		ft_wstrlen(const wchar_t *s)
{
	size_t length;

	length = 0;
	while (*s)
	{
		length += ft_size(*s);
		s++;
	}
	return (length);
}
