/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrsub.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgros <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/11 11:25:40 by tgros             #+#    #+#             */
/*   Updated: 2016/12/20 15:07:11 by tgros            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	size_wchar(wchar_t c)
{
	int mask;
	int size;

	mask = 2147483648;
	size = 32;
	while ((c & mask) == 0 && size != 0)
	{
		size--;
		mask = mask >> 1;
	}
	if (size <= 7)
		return (1);
	else if (size <= 11)
		return (2);
	else if (size <= 16)
		return (3);
	else
		return (4);
}

wchar_t		*ft_wstrsub(wchar_t const *s, unsigned int start, size_t len)
{
	wchar_t	*str;
	wchar_t *tmp;
	size_t	i;

	if (!s)
		return (NULL);
	i = 0;
	str = (wchar_t *)malloc(sizeof(wchar_t) * (len + 1));
	if (!str)
		return (NULL);
	tmp = str;
	while (i < len)
	{
		if (i + size_wchar(*(s + start)) <= len)
		{
			*str = *s;
			s++;
			str++;
		}
		i += size_wchar(*s);
	}
	*str = '\0';
	return (tmp);
}
